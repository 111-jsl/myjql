#include "table.h"

#include "hash_map.h"

#include <stdio.h>
// #include <stdlib.h>

void table_init(Table *table, const char *data_filename, const char *fsm_filename) {
    init_buffer_pool(data_filename, &table->data_pool);
    hash_table_init(fsm_filename, &table->fsm_pool, PAGE_SIZE / HASH_MAP_DIR_BLOCK_SIZE);
}

void table_close(Table *table) {
    close_buffer_pool(&table->data_pool);
    hash_table_close(&table->fsm_pool);
}

off_t table_get_total_blocks(Table *table) {
    return table->data_pool.file.length / PAGE_SIZE;
}

short table_block_get_total_items(Table *table, off_t block_addr) {
    Block *block = (Block*)get_page(&table->data_pool, block_addr);
    short n_items = block->n_items;
    release(&table->data_pool, block_addr);
    return n_items;
}

void table_read(Table *table, RID rid, ItemPtr dest) {
    // printf("table read\n");
    if(table->data_pool.tail_addr <= get_rid_block_addr(rid)){
        dest = NULL;
        return;
    }
    Block *block = (Block *)get_page(&table->data_pool, get_rid_block_addr(rid));
    // printf("block size: %d\n", get_item_id_size(get_item_id(block, get_rid_idx(rid))));
    // print_rid(rid);
    ItemPtr shallow_copy = get_item(block, get_rid_idx(rid));
    // system("pause");
    if(shallow_copy != NULL){
        short item_size = get_item_id_size(get_item_id(block, get_rid_idx(rid)));
        // dest = (char*)malloc(sizeof(char) * item_size);
        for(int i = 0; i < item_size; i++){
            dest[i] = shallow_copy[i];
        }
    }
    
    release(&table->data_pool, get_rid_block_addr(rid));
    // system("pause");
}

RID table_insert(Table *table, ItemPtr src, short size) {
    // printf("table insert\n");
    // printf("size: %d\n", size);
    // printer_t printer = str_printer;
    // print_table(table, printer);
    RID rid;
    get_rid_block_addr(rid) = -1;
    get_rid_idx(rid) = -1;
    int append_block = 0;
    off_t dest_addr = hash_table_pop_lower_bound(&table->fsm_pool, size);
    
    if(dest_addr == -1){
        append_block = 1;
        dest_addr = table->data_pool.tail_addr;
        
    }
    // printf("pop lower bound: %d\n", dest_addr);
    // if(dest_addr == -1){
    //     printf("table insert error: no more space for size: %hd", size);
    //     return rid;
    // }
    Block *block = (Block *)get_page(&table->data_pool, dest_addr);
    if(append_block) init_block(block);
    short idx = new_item(block, src, size - sizeof(ItemID));

    // printf("block after insert\n");
    // printer_t printer = str_printer;
    // print_block(block, printer);


    int have_hole = (block->head_ptr - 3 * sizeof(short)) / sizeof(ItemID) - block->n_items;
    size = block->tail_ptr - block->head_ptr;
    if(have_hole) size += sizeof(ItemID);
    // printf("hash table insert: size: %d\n", size);
    hash_table_insert(&table->fsm_pool, size, dest_addr);
    get_rid_block_addr(rid) = dest_addr;
    get_rid_idx(rid) = idx;
    release(&table->data_pool, dest_addr);
    // print_table(table, printer);
    return rid;   
}

void table_delete(Table *table, RID rid) {
    Block *block = (Block *)get_page(&table->data_pool, get_rid_block_addr(rid));
    short size = 0;
    int have_hole = (block->head_ptr - 3 * sizeof(short)) / sizeof(ItemID) - block->n_items;
    size = block->tail_ptr - block->head_ptr;
    if(have_hole) size += sizeof(ItemID);

    hash_table_pop(&table->fsm_pool, size, get_rid_block_addr(rid));
    delete_item(block, get_rid_idx(rid));

    have_hole = (block->head_ptr - 3 * sizeof(short)) / sizeof(ItemID) - block->n_items;
    size = block->tail_ptr - block->head_ptr;
    if(have_hole) size += sizeof(ItemID);

    release(&table->data_pool, get_rid_block_addr(rid));
    // printf("delete size: %d\n", size);
    hash_table_insert(&table->fsm_pool, size, get_rid_block_addr(rid));
}

void print_table(Table *table, printer_t printer) {
    printf("\n---------------TABLE---------------\n");
    off_t i, total = table_get_total_blocks(table);
    off_t block_addr;
    Block *block;
    for (i = 0; i < total; ++i) {
        block_addr = i * PAGE_SIZE;
        block = (Block*)get_page(&table->data_pool, block_addr);
        printf("[" FORMAT_OFF_T "]\n", block_addr);
        print_block(block, printer);
        release(&table->data_pool, block_addr);
    }
    printf("***********************************\n");
    print_hash_table(&table->fsm_pool);
    printf("-----------------------------------\n\n");
}

void print_rid(RID rid) {
    printf("RID(" FORMAT_OFF_T ", %d)", get_rid_block_addr(rid), get_rid_idx(rid));
}

void analyze_table(Table *table) {
    block_stat_t stat, curr;
    off_t i, total = table_get_total_blocks(table);
    off_t block_addr;
    Block *block;
    stat.empty_item_ids = 0;
    stat.total_item_ids = 0;
    stat.available_space = 0;
    for (i = 0; i < total; ++i) {
        block_addr = i * PAGE_SIZE;
        block = (Block*)get_page(&table->data_pool, block_addr);
        analyze_block(block, &curr);
        release(&table->data_pool, block_addr);
        accumulate_stat_info(&stat, &curr);
    }
    printf("++++++++++ANALYSIS++++++++++\n");
    printf("total blocks: " FORMAT_OFF_T "\n", total);
    total *= PAGE_SIZE;
    printf("total size: " FORMAT_OFF_T "\n", total);
    printf("occupancy: %.4f\n", 1. - 1. * stat.available_space / total);
    printf("ItemID occupancy: %.4f\n", 1. - 1. * stat.empty_item_ids / stat.total_item_ids);
    printf("++++++++++++++++++++++++++++\n\n");
}