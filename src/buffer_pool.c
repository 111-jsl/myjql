#include "buffer_pool.h"
#include "file_io.h"

#include <stdio.h>
#include <stdlib.h>

void init_buffer_pool(const char *filename, BufferPool *pool) {
    printf("call init_buffer_pool\n");
    open_file(&pool->file, filename);
   
    pool->tail_addr = pool->file.length;
    for(int i = 0; i < CACHE_PAGE; i++){
       
        pool->addrs[i] = -1;
        pool->ref[i] = 0;

        
        pool->cnt[i] = i + 1;
    }

}

void close_buffer_pool(BufferPool *pool) {
    for(int i = 0; i < CACHE_PAGE; i++){
        if(pool->ref[i]){
            printf("close buffer pool error: transaction still using page\n");
            return;
        }
    }
    for(int i = 0; i < CACHE_PAGE; i++){
        write_page(&pool->pages[i], &pool->file, pool->addrs[i]);
    }
    close_file(&pool->file);

}

Page *get_page(BufferPool *pool, off_t addr) {
    // printf("call get_page\n");
    // printf("---------getpage---------\n");
    // printf("addr: %d\n", addr);
   
    // printf("//\n");
    // print_buffer_pool(pool);
    // // system("pause");
    // if(addr == -1){
    //     printf("weirdo\n");
    //     system("pause");
    // }
    // if(addr > pool->tail_addr){
    //     printf("addr out of range\n");
    //     system("pause");
    // }
    size_t max_age = 0;
    int replace_target = 0;
    for(int i = 0; i < CACHE_PAGE; i++){
        ++pool->cnt[i];
        if(pool->ref[i]) pool->cnt[i] = 1;
        if(pool->cnt[i] == 0){
            for(int j = 0; j < CACHE_PAGE; j++){
                if(pool->ref[j]) pool->cnt[j] = 1;
                else{
                    pool->cnt[j] = 2;
                    replace_target = j;
                }
            }
            break;
        }
        
        if(pool->cnt[i] > max_age){
            replace_target = i;
            max_age = pool->cnt[i];
        }
    }
    if(pool->ref[replace_target]){
        printf("error: no free page in buffer pool\n");
        // system("pause");
    }
    
   
    if(pool->tail_addr == addr){
        if(pool->addrs[replace_target] != -1)
            write_page(&pool->pages[replace_target], &pool->file, pool->addrs[replace_target]);
        off_t init_page[CACHE_PAGE];
        write_page(init_page, &pool->file, addr);
        pool->tail_addr += PAGE_SIZE;
        read_page(&pool->pages[replace_target], &pool->file, addr);

        pool->addrs[replace_target] = addr;
        pool->ref[replace_target] = 1;
        pool->cnt[replace_target] = 1;
        return &pool->pages[replace_target];
    }


    for(int i = 0; i < CACHE_PAGE; i++){

       
        if(addr == pool->addrs[i]){
         
            pool->ref[i]++;
            pool->cnt[i] = 1;
          
            return &pool->pages[i];
        }
    }
    if(pool->addrs[replace_target] != -1)
        write_page(&pool->pages[replace_target], &pool->file, pool->addrs[replace_target]);
    
    read_page(&pool->pages[replace_target], &pool->file, addr);


    pool->addrs[replace_target] = addr;
    pool->ref[replace_target] = 1;
    pool->cnt[replace_target] = 1;

    // print_buffer_pool(pool);
    return &pool->pages[replace_target];
}

void release(BufferPool *pool, off_t addr) {
    // printf("------release-------\n");
    // printf("addr: %d\n", addr);
    for(int i = 0; i < CACHE_PAGE; i++){
        if(addr == pool->addrs[i]){
            if(pool->ref[i]) pool->ref[i]--;
            return;
        }
    }
    printf("release error!\n");
}

void print_buffer_pool(BufferPool *pool) {
    printf("-------buffer_pool--------\n");
    printf("-------addr--------\n");
    for(int i = 0 ; i < CACHE_PAGE; i++){
       printf("addr[%d]: %d\n", i, pool->addrs[i]);
    }
    printf("-------cnt--------\n");
    for(int i = 0 ; i < CACHE_PAGE; i++){
       printf("cnt[%d]: %zd\n", i, pool->cnt[i]);
    }
    printf("-------ref--------\n");
    for(int i = 0 ; i < CACHE_PAGE; i++){
       printf("ref[%d]: %zd\n", i, pool->ref[i]);
    }
    printf("-------tail--------\n");
    printf("%d\n", pool->tail_addr);
    // for(int i = 0 ; i < CACHE_PAGE; i++){
    //    printf("cnt_sort[%d]: %zd\n", i, pool->cnt_sort[i]);
    // }
    // system("pause");
}

// void validate_buffer_pool(BufferPool *pool) {
// }
