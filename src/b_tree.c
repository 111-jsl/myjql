#include "b_tree.h"
#include "buffer_pool.h"

#include <stdio.h>
#include <stdlib.h>

void b_tree_init(const char *filename, BufferPool *pool) {
    init_buffer_pool(filename, pool);
    /* TODO: add code here */
    if(pool->tail_addr == 0){
        printf("first\n");
        BCtrlBlock *bctrl = (BCtrlBlock*)get_page(pool, pool->tail_addr);
        bctrl->root_node = pool->tail_addr;
        bctrl->free_node_head = -1;
        
        BNode *ini_bnode = (BNode*)get_page(pool, pool->tail_addr);
        ini_bnode->n = 0;
        ini_bnode->next = -1;
        ini_bnode->leaf = 1;
        release(pool, bctrl->root_node);
        release(pool, 0);
    }
}

void b_tree_close(BufferPool *pool) {
    // for(int i = 0; i < CACHE_PAGE; i++){
    //     printf("pool page[%d]: %d\n", i, pool->addrs[i]);
    //     printf("pool ref[%d]: %d\n", i, pool->ref[i]);
    // }
    close_buffer_pool(pool);
}

RID b_tree_search(BufferPool *pool, void *key, size_t size, b_tree_ptr_row_cmp_t cmp) {
    BCtrlBlock *bctrl = (BCtrlBlock*)get_page(pool, 0);
    off_t root = bctrl->root_node;
    release(pool, 0);
    printf("root addr: %d\n", root);
    BNode *now = (BNode*)get_page(pool, root);
    off_t now_addr = root;
    off_t chosen_child = -1;
    while(now->leaf == 0){
        int i = 0;
        
        for(; i < now->n; i++){
            if(cmp(key, size, now->row_ptr[i]) < 0){
                chosen_child = now->child[i];
                break;
            }
        }
        if(i == now->n) chosen_child = now->child[i];
        // printf("choose path i: %d\n", i);
        release(pool, now_addr);
        now_addr = chosen_child;
        now = (BNode*)get_page(pool, chosen_child);
    }
    // printf("b_tree search checkpoint\n");
    // printf("now->n: %d\n", now->n);
    // system("pause");
    int i = 0;
    for(; i < now->n; i++){
        // printf("now->row_ptr[%d]: %d\n", i, get_rid_block_addr(now->row_ptr[i]));
        if(cmp(key, size, now->row_ptr[i]) == 0) break;
    }
    // system("pause");
    if(i == now->n){
        // printf("b_tree_search warning: index not found\n");
        release(pool, now_addr);
        RID rid;
        get_rid_block_addr(rid) = -1;
        get_rid_idx(rid) = 0;
        return rid;
    }
    RID rid;
    rid = now->row_ptr[i];
    release(pool, now_addr);
    return rid;

}

off_t b_tree_insert_rc(BufferPool *pool, RID rid, b_tree_row_row_cmp_t cmp, off_t child, RID *pop, RID *replace){
    BNode *now = (BNode*)get_page(pool, child);
    off_t chosen_child = -1;
    off_t new_childnode = -1;
    off_t ret = -1;
    // int i = 0;
    if(now->leaf == 0){
        int i = 0;
        for(; i < now->n; i++){
            if(cmp(rid, now->row_ptr[i]) < 0){
                chosen_child = now->child[i];
                break;
            }
        }
        if(i == now->n) chosen_child = now->child[i];
        release(pool, child);
        new_childnode = b_tree_insert_rc(pool, rid, cmp, chosen_child, pop, replace);
        
        //get page again
        now = (BNode*)get_page(pool, child);
        if(get_rid_block_addr(*replace) > 0 && i - 1 >= 0 && cmp(*replace, now->row_ptr[i - 1]) == 0){
            now->row_ptr[i - 1] = *replace;
        }
        

        if(new_childnode != -1){
            
            // insert new child node
            RID overflow_rid;
            off_t overflow_off;
            if(i == now->n){
                overflow_off = new_childnode;
                overflow_rid = *pop;
            }
            else{
                overflow_off = now->child[now->n];
                overflow_rid = now->row_ptr[now->n - 1];
                for(int j = now->n - 1; j > i; j--){
                    now->child[j + 1] = now->child[j];
                    now->row_ptr[j] = now->row_ptr[j - 1];
                
                }
                // BNode *new_child = (BNode*)get_page(pool, new_childnode);
                now->child[i + 1] = new_childnode;
                now->row_ptr[i] = *pop;
            
            }
            // release(pool, new_childnode);
            if(now->n == 2 * DEGREE - 1){
                //get new page
                BNode *newpage = NULL;
                BCtrlBlock *bctrl = (BCtrlBlock*)get_page(pool, 0);
                if(bctrl->free_node_head == -1){
                    ret = pool->tail_addr;
                    newpage = (BNode*)get_page(pool, pool->tail_addr);
                }
                else{
                    ret = bctrl->free_node_head;
                    newpage = (BNode*)get_page(pool, bctrl->free_node_head);
                    bctrl->free_node_head = newpage->next;
                    
                }
                release(pool, 0);
                newpage->next = now->next;
                now->next = ret; // now --> new child node
                // assign new page
                int j, k;
                for(j = DEGREE + 1, k = 0; j < now->n; j++, k++){
                    newpage->child[k] = now->child[j];
                    newpage->row_ptr[k] = now->row_ptr[j];
                   
                }
                *pop = now->row_ptr[DEGREE];
               
                newpage->child[k] = now->child[j];
                newpage->child[k + 1] = overflow_off;
                newpage->row_ptr[k] = overflow_rid;
               
                newpage->n = k + 1;
                newpage->leaf = 0;
                now->n = DEGREE;
                release(pool, ret);
            }
            else{
                now->row_ptr[now->n] = overflow_rid;
                now->child[now->n + 1] = overflow_off;
                now->n++;

            }
            
        }
    }
    else{
        int i = 0;
        for(i = 0; i < now->n; i++){
            if(cmp(rid, now->row_ptr[i]) < 0) break;
        }
        RID overflow_rid;
        if(i == now->n){
            overflow_rid = rid;
            
        }
        else{
            overflow_rid = now->row_ptr[now->n - 1];
            
            for(int j = now->n - 1; j > i; j--){
                now->row_ptr[j] = now->row_ptr[j - 1];
            
            }
            now->row_ptr[i] = rid;
        
        }
        if(now->n == 2 * DEGREE - 1){
            //get new page
            // printf("insert get new page\n");
            // system("pause");
            BNode *newpage = NULL;
            BCtrlBlock *bctrl = (BCtrlBlock*)get_page(pool, 0);
            if(bctrl->free_node_head == -1){
                ret = pool->tail_addr;
                newpage = (BNode*)get_page(pool, pool->tail_addr);
            }
            else{
                ret = bctrl->free_node_head;
                newpage = (BNode*)get_page(pool, bctrl->free_node_head);
                bctrl->free_node_head = newpage->next;
                
            }
            release(pool, 0);
            newpage->next = now->next;
            now->next = ret; // now --> new child node
            // assign new page
            int j, k;
            for(j = DEGREE, k = 0; j < now->n; j++, k++){
                newpage->row_ptr[k] = now->row_ptr[j];
                
            }
            newpage->row_ptr[k] = overflow_rid;
            
            newpage->n = k + 1;
            newpage->leaf = 1;
            now->n = DEGREE;
            *pop = newpage->row_ptr[0];
            
            
            release(pool, ret);
        }
        else{
            now->row_ptr[now->n] = overflow_rid;
           
            
            now->n++;

        }
        if(i == 0 && now->n > 0){
            *replace = now->row_ptr[1];
            
        }
        
        

    }
    release(pool, child);
    // printf("ret: %d\n", ret);
    return ret;
}



RID b_tree_insert(BufferPool *pool, RID rid, b_tree_row_row_cmp_t cmp, b_tree_insert_nonleaf_handler_t insert_handler){
    //keep open to get free pages
    BCtrlBlock *bctrl = (BCtrlBlock*)get_page(pool, 0);
    off_t root = bctrl->root_node, 
          new_childnode = -1, 
          root_nxt = 0;
    release(pool, 0);
    RID *pop = (RID*)malloc(sizeof(RID));
    RID *replace = (RID*)malloc(sizeof(RID));
    get_rid_block_addr(*replace) = -2;
    get_rid_idx(*replace) = -2;
    get_rid_block_addr(*pop) = -2;
    get_rid_idx(*pop) = -2;
    new_childnode = b_tree_insert_rc(pool, rid, cmp, root, pop, replace);
    // printf("insert complete, *pop: %d\n", get_rid_block_addr(*pop));
    if(new_childnode != -1){
        //get new root
        BNode *newpage = NULL;
        bctrl = (BCtrlBlock*)get_page(pool, 0);
        if(bctrl->free_node_head == -1){
            root_nxt = pool->tail_addr;
            newpage = (BNode*)get_page(pool, pool->tail_addr);
        }
        else{
            root_nxt = bctrl->free_node_head;
            newpage = (BNode*)get_page(pool, root_nxt);
            bctrl->free_node_head = newpage->next;
        }
        // BNode *new_child = (BNode*)get_page(pool, new_childnode);
        newpage->next = -1;
        newpage->leaf = 0;
        newpage->n = 1;
        newpage->child[0] = root;
        newpage->child[1] = new_childnode;
        newpage->row_ptr[0] = *pop;
       
        release(pool, root_nxt);
        // bctrl = (BCtrlBlock*)get_page(pool, 0);
        bctrl->root_node = root_nxt;
        release(pool, 0);
    }
    free(replace);
    free(pop);
    // b_tree_traverse(pool);
    return rid;
}

off_t b_tree_delete_rc(BufferPool *pool, RID rid, b_tree_row_row_cmp_t cmp, off_t child, RID *replace){
    BNode *now = (BNode*)get_page(pool, child);
    off_t chosen_child = -1;
    off_t old_childnode = -1;
    off_t ret = -1;
    // int i = 0;
    if(now->leaf == 0){
        int i = 0;
        for(; i < now->n; i++){
            if(cmp(rid, now->row_ptr[i]) < 0){
                chosen_child = now->child[i];
                break;
            }
        }
        if(i == now->n) chosen_child = now->child[i];
        release(pool, child);
        old_childnode = b_tree_delete_rc(pool, rid, cmp, chosen_child, replace);

        //get page again
        now = (BNode*)get_page(pool, child);
        //maintain node
        // printf("same node maintain: %d\n", get_rid_block_addr(now->row_ptr[i - 1]));
        if(i - 1 >= 0 && cmp(rid, now->row_ptr[i - 1]) == 0){
            now->row_ptr[i - 1] = *replace;
        }
        
        // BNode *rb = (BNode*)get_page(pool, chosen_child);
        // if(i - 1 >= 0 && cmp(*pop, now->row_ptr[i - 1]) == 0){
        //     get_rid_block_addr(now->row_ptr[i - 1]) = get_rid_block_addr()
        // }
        if(old_childnode != -1){
            
            // left redistribution ?
            BNode *old_child = (BNode*)get_page(pool, old_childnode);
            BNode *left_brother = NULL, *right_brother = NULL;
            off_t left_bronode = -1, right_bronode = -1;
            if(i - 1 >= 0){
                left_brother = (BNode*)get_page(pool, now->child[i - 1]);
                left_bronode = now->child[i - 1];
            }
            if(i + 1 <= now->n){
                right_brother = (BNode*)get_page(pool, now->child[i + 1]);
                right_bronode = now->child[i + 1];
            }

            if(left_brother != NULL && left_brother->n >= DEGREE){
                // printf("left redist\n");
                old_child->child[old_child->n + 1] = old_child->child[old_child->n];
                for(int j = old_child->n - 1; j >= 0; j--){
                    old_child->child[j + 1] = old_child->child[j];
                    old_child->row_ptr[j + 1] = old_child->row_ptr[j];
                }
                old_child->child[0] = left_brother->child[left_brother->n];
                if(old_child->leaf == 1){
                    old_child->row_ptr[0] = left_brother->row_ptr[left_brother->n - 1];
                    now->row_ptr[i - 1] = old_child->row_ptr[0];
                }
                else{
                    old_child->row_ptr[0] = now->row_ptr[i - 1];
                    now->row_ptr[i - 1] = left_brother->row_ptr[left_brother->n - 1];
                }
                
                left_brother->n--;
                old_child->n++;
                // if(old_child->leaf == 0){
                //     b_tree_traverse(pool);
                //     system("pause");
                // }
            }
            // right redist ?
            else if(right_brother != NULL && right_brother->n >= DEGREE){
                // printf("right redist\n");
                if(old_child->leaf == 1){
                    old_child->row_ptr[old_child->n] = right_brother->row_ptr[0];
                    now->row_ptr[i] = right_brother->row_ptr[1];
                }
                else{
                    old_child->row_ptr[old_child->n] = now->row_ptr[i];
                    now->row_ptr[i] = right_brother->row_ptr[0];
                    old_child->child[old_child->n + 1] = right_brother->child[0];
                }
                old_child->n++;
                //backward
                int j = 0;
                for(; j < right_brother->n - 1; j++){
                    right_brother->child[j] = right_brother->child[j + 1];
                    right_brother->row_ptr[j] = right_brother->row_ptr[j + 1];
                }
                right_brother->child[j] = right_brother->child[j + 1];
                right_brother->n--;
                // if(old_child->leaf == 0){
                //     b_tree_traverse(pool);
                //     system("pause");
                // }
            }
            // left merge ?
            else if(left_brother != NULL && left_brother->n < DEGREE){
                // printf("left merge\n");
                if(old_child->leaf == 1){
                    left_brother->n--;
                }
                else{
                    left_brother->row_ptr[left_brother->n] = now->row_ptr[i - 1];
                }
                int j = 0;
                for(; j < old_child->n; j++){
                    left_brother->child[left_brother->n + 1 + j] = old_child->child[j];
                    left_brother->row_ptr[left_brother->n + 1 + j] = old_child->row_ptr[j];
                   
                }
                left_brother->child[left_brother->n + 1 + j] = old_child->child[j];
                for(int j = i; j < now->n; j++){
                    now->child[j] = now->child[j + 1];
                    now->row_ptr[j - 1] = now->row_ptr[j];
        
                }
                
                if(--now->n < DEGREE - 1){
                    ret = child;
                }
                left_brother->n += 1 + old_child->n;
                BCtrlBlock *bctrl = (BCtrlBlock*)get_page(pool, 0);
                left_brother->next = old_child->next;
                old_child->next = bctrl->free_node_head;
                bctrl->free_node_head = old_childnode;
                release(pool, 0);
                
                // if(old_child->leaf == 0){
                //     b_tree_traverse(pool);
                //     system("pause");
                // }
            }
            // right merge ?
            else if(right_brother != NULL && right_brother->n < DEGREE){
                // printf("right merge\n");
                if(old_child->leaf == 1){
                    old_child->n--;
                }
                else{
                    old_child->row_ptr[old_child->n] = now->row_ptr[i];
                }
                int j = 0;
                for(; j < right_brother->n; j++){
                    old_child->child[old_child->n + 1 + j] = right_brother->child[j];
                    old_child->row_ptr[old_child->n + 1 + j] = right_brother->row_ptr[j];
                }
                old_child->child[old_child->n + 1 + j] = right_brother->child[j];
                for(int j = i + 1; j < now->n; j++){
                    now->child[j] = now->child[j + 1];
                    now->row_ptr[j - 1] = now->row_ptr[j];
                }

                if(--now->n < DEGREE - 1){
                    ret = child;
                    
                }
                old_child->n += 1 + right_brother->n;
                BCtrlBlock *bctrl = (BCtrlBlock*)get_page(pool, 0);
                old_child->next = right_brother->next;
                right_brother->next = bctrl->free_node_head;
                bctrl->free_node_head = right_bronode;
                release(pool, 0);
                // if(old_child->leaf == 0){
                //     b_tree_traverse(pool);
                //     system("pause");
                // }
            }
            else{
                printf("b_tree_delete error: fatal\n");
                system("pause");
                return -1;
            }
            // delete old child node
        
            
            
            if(left_bronode != -1)
                release(pool, left_bronode);
            if(right_bronode != -1)
                release(pool, right_bronode);
            
            
            release(pool, old_childnode);
            
            
        }
        
    }
    else{
        // system("pause");
        int i = 0;
        // printf("how many: %d\n", now->n);
        for(i = 0; i < now->n; i++){
            // printf("delete now->ptr[%d]: %d\n", i, get_rid_block_addr(now->row_ptr[i]));
            if(cmp(rid, now->row_ptr[i]) == 0) break;
        }
        // system("pause");
        if(i == now->n){
            // printf("b_tree delete warning: delete target not found\n");
            return -1;
        }
        if(i + 1 < now->n){
            *replace = now->row_ptr[i + 1];
        }
        for(int j = i + 1; j < now->n; j++){
            now->row_ptr[j - 1] = now->row_ptr[j];
        }
        if(--now->n < DEGREE - 1){
            ret = child;
        }
        

    }
    // if(ret != -1)
        // printf("ret first element: %d\n", get_rid_block_addr(now->row_ptr[0]));
    release(pool, child);
    // return new child node addr
    // printf("delete ret: %d\n", ret);
    return ret;

}



void b_tree_delete(BufferPool *pool, RID rid, b_tree_row_row_cmp_t cmp, b_tree_insert_nonleaf_handler_t insert_handler, b_tree_delete_nonleaf_handler_t delete_handler) {

    BCtrlBlock *bctrl = (BCtrlBlock*)get_page(pool, 0);
    off_t root = bctrl->root_node;
    release(pool, 0);
    // release(pool, 0);
    // BNode *now = (BNode*)get_page(pool, root);
    RID *replace = (RID*)malloc(sizeof(RID));
    get_rid_block_addr(*replace) = -2;
    get_rid_idx(*replace) = -2;
    
    off_t old_rootnode = b_tree_delete_rc(pool, rid, cmp, root, replace);
    free(replace);
    if(old_rootnode != -1){
        BNode *old_root = (BNode*)get_page(pool, old_rootnode);
        bctrl = (BCtrlBlock*)get_page(pool, 0);
        if(old_root->n == 0 && old_root->leaf == 0){
            old_root->next = bctrl->free_node_head;
            bctrl->free_node_head = old_rootnode;
            bctrl->root_node = old_root->child[0];
        }
        release(pool, old_rootnode);
        release(pool, 0);
    }


}

void b_tree_traverse(BufferPool *pool){
    BCtrlBlock *bctrl = (BCtrlBlock*)get_page(pool, 0);
    off_t root_addr = bctrl->root_node;
    release(pool, 0);
    printf("root addr: %d\n", root_addr);
    int cnt = 0;
    dfs(pool, root_addr, cnt);
}

void dfs(BufferPool *pool, off_t now, int cnt){
    if(cnt > 10000){
        printf("list too long\n");
        system("pause");
    }
    BNode *node = (BNode*)get_page(pool, now);
    printf("check num: %d\n", node->n);
    if(node->n > 2 * DEGREE - 1){
        printf("node currupt\n");
        system("pause");
    }
    if(node->leaf == 1){
        for(int i = 0; i < node->n; i++){
            printf("key: %d\n", get_rid_block_addr(node->row_ptr[i]));
        }
        printf("--------------------------\n");
    }
    if(node->leaf == 1){
        release(pool, now);
        return;
    }
    off_t num = node->n;
    for(int i = 0; i <= num; i++){
        off_t addr = node->child[i];
        release(pool, now);
        dfs(pool, addr, cnt + 1);
        node = (BNode*)get_page(pool, now);
    }
    release(pool, now);
}