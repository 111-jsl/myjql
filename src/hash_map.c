#include "hash_map.h"

#include <stdio.h>
#include <stdlib.h>

void hash_table_init(const char *filename, BufferPool *pool, off_t n_directory_blocks) {
    
   
    init_buffer_pool(filename, pool);
    printf("length: %d\n", pool->file.length);
   
    if(pool->file.length == 0){
        
        /* TODO: add code here */
        
        
     
        HashMapControlBlock *ctrl = (HashMapControlBlock*)get_page(pool, pool->tail_addr);
     

        ctrl->n_directory_blocks = n_directory_blocks;
        ctrl->max_size = n_directory_blocks * HASH_MAP_DIR_BLOCK_SIZE;
        ctrl->free_block_head = -1;
        for(int i = 0; i < 8; i++){
            ctrl->dirblock_nxt[i] = -1;
            ctrl->dirblock_num[i] = 0;
        }
        release(pool, 0);
        for(int i = 0; i < n_directory_blocks; i++){
           
            off_t tail = pool->tail_addr;
            HashMapDirectoryBlock *dir = (HashMapDirectoryBlock*)get_page(pool, pool->tail_addr);
           
            for(int j = 0; j < HASH_MAP_DIR_BLOCK_SIZE; j++){
                dir->directory[j] = -1;
               
            }
            release(pool, tail);
         
        }
      
    }

}




void hash_table_close(BufferPool *pool) {
    close_buffer_pool(pool);
}




void hash_table_insert(BufferPool *pool, short size, off_t addr) {
    // printf("hash table insert addr: %d\n", addr);
    int index = size / HASH_MAP_DIR_BLOCK_SIZE;
    int offset = size % HASH_MAP_DIR_BLOCK_SIZE;
    HashMapBlock *now = NULL;
    
    HashMapDirectoryBlock *dir = (HashMapDirectoryBlock*)get_page(pool, (1 + index) * PAGE_SIZE);
    off_t laddr = dir->directory[offset];
    int new_size_append = 0;

  
    if(laddr != -1){
  
        now = (HashMapBlock*)get_page(pool, laddr);
        if(now->n_items < HASH_MAP_BLOCK_SIZE){
            now->table[now->n_items++] = addr;
            release(pool, (1 + index) * PAGE_SIZE);
            // release(pool, 0);
            release(pool, laddr);
            // print_hash_table(pool);
            return;
        }
        release(pool, laddr);
    }
    else{
        new_size_append = 1;
    }

    // new size append, maintain dirblock_nxt
    HashMapControlBlock *ctrl = (HashMapControlBlock*)get_page(pool, 0);
    for(int i = 0; i < index; i++){
        if(ctrl->dirblock_nxt[i] == -1 || ctrl->dirblock_nxt[i] > index) ctrl->dirblock_nxt[i] = index;
    }
    if(new_size_append){
        ctrl->dirblock_num[index]++;
    }


    off_t new_block_laddr;
    if(ctrl->free_block_head == -1){
        new_block_laddr = pool->tail_addr;
    }
    else{
        new_block_laddr = ctrl->free_block_head;
        HashMapBlock *tmp = (HashMapBlock*)get_page(pool, new_block_laddr);
        ctrl->free_block_head = tmp->next;
        release(pool, new_block_laddr);
    }
    release(pool, 0);

    HashMapBlock *new_block = (HashMapBlock*)get_page(pool, new_block_laddr);
    
    new_block->next = dir->directory[offset];
    dir->directory[offset] = new_block_laddr;
    // printf("new_block_laddr: %d\n", new_block_laddr);

    new_block->n_items = 0;
    new_block->table[new_block->n_items++] = addr;
    release(pool, new_block_laddr);
    release(pool, (1 + index) * PAGE_SIZE);
    // printf("n_items: %d\n", new_block->n_items);
    
    // print_hash_table(pool);
    
    // for(int i = 0; i < 8; i++){
    //     printf("nxt[%d]: %d\n", i, ctrl->dirblock_nxt[i]);
    // }
    // print_hash_table(pool);
}




off_t hash_table_pop_lower_bound(BufferPool *pool, short size) {
    
    HashMapControlBlock *ctrl = (HashMapControlBlock*)get_page(pool, 0);
    
    // for(int i = 0; i < 8; i++){
    //     printf("nxt[%d]: %d\n", i, ctrl->dirblock_nxt[i]);
    // }
   
    //search lower bound
    int index = size / HASH_MAP_DIR_BLOCK_SIZE;
    int offset = size % HASH_MAP_DIR_BLOCK_SIZE;
    off_t dir_addr = (1 + index) * PAGE_SIZE;
    HashMapDirectoryBlock *dir = (HashMapDirectoryBlock*)get_page(pool, dir_addr);
    for(; offset < HASH_MAP_DIR_BLOCK_SIZE; offset++){
        // printf("offset: %d\n", offset);
        if(dir->directory[offset] != -1){
            break;
        }
    }

    if(offset == HASH_MAP_DIR_BLOCK_SIZE){
        release(pool, dir_addr);
        if(ctrl->dirblock_nxt[index] == -1){
            release(pool, 0);
            return -1;
        }
        dir_addr = (1 + ctrl->dirblock_nxt[index]) * PAGE_SIZE;
        dir = (HashMapDirectoryBlock*)get_page(pool, dir_addr);
        int i = 0;
        for(; i < HASH_MAP_DIR_BLOCK_SIZE; i++){
            if(dir->directory[i] != -1){
                offset = i;
                index = ctrl->dirblock_nxt[index];
                break;
            }
        }
        if(i == HASH_MAP_DIR_BLOCK_SIZE){
            printf("dirblock_nxt crash\n");
            system("pause");
        }
    }

    
    
    off_t laddr = dir->directory[offset];
    // int require_maintain = 1;


    HashMapBlock *now = (HashMapBlock*)get_page(pool, laddr);
    off_t ret = now->table[now->n_items - 1];
    if(now->n_items - 1 == 0){
        off_t tmp = ctrl->free_block_head;
        ctrl->free_block_head = dir->directory[offset];
        dir->directory[offset] = now->next;
        now->n_items = 0;
        now->next = tmp;
        if(dir->directory[offset] == -1){
            ctrl->dirblock_num[index]--;
            if(ctrl->dirblock_num[index] == 0){
                for(int i = 0; i < index; i++){
                    if(ctrl->dirblock_nxt[i] == -1 || ctrl->dirblock_nxt[i] == index){
                        ctrl->dirblock_nxt[i] = ctrl->dirblock_nxt[index];
                    }
                }
            }
        }
        
    }
    else{
        now->n_items--;
    }
    
    release(pool, laddr);
    release(pool, 0);
    release(pool, dir_addr);
    // print_hash_table(pool);
    // printf("ret: %d\n", ret);
    // print_hash_table(pool);
    return ret;
}

void hash_table_pop(BufferPool *pool, short size, off_t addr) {
    // print_hash_table(pool);
    // system("pause");
    // system("pause");
    
    int index = size / HASH_MAP_DIR_BLOCK_SIZE;
    int offset = size % HASH_MAP_DIR_BLOCK_SIZE;
    HashMapDirectoryBlock *dir = (HashMapDirectoryBlock*)get_page(pool, (1 + index) * PAGE_SIZE);
    HashMapBlock *now = NULL;

  
    off_t now_addr = dir->directory[offset];
    // off_t prepre_addr = dir->directory[offset], _prepre_addr = 0;
    if(now_addr == -1){
        release(pool, (1 + index) * PAGE_SIZE);
        return;
    }
    int hash_hit = 0;
    int kick_page = 0;
   
    
    now = (HashMapBlock*)get_page(pool, now_addr);
    if(now->n_items == 1) kick_page = 1;
    off_t tmp = now->table[now->n_items-1];
    release(pool, now_addr);

    for(; now_addr != -1; ){
        now = (HashMapBlock*)get_page(pool, now_addr);
        for(int i = 0; i < now->n_items; i++){
            if(addr == now->table[i]){
                hash_hit = 1;
                now->table[i] = tmp;
                break;
            }
        }
        
        off_t _now_addr = now->next;
        release(pool, now_addr);
        now_addr = _now_addr;
        if(hash_hit == 1) break;
    }
    if(hash_hit == 0){
        release(pool, (1 + index) * PAGE_SIZE);
        return;
    }
    HashMapBlock* target = (HashMapBlock*)get_page(pool, dir->directory[offset]);
    off_t target_addr = dir->directory[offset];
    
    target->n_items--;
    if(kick_page){
        HashMapControlBlock *ctrl = (HashMapControlBlock*)get_page(pool, 0);
        off_t target_nxt = target->next;
        target->next = ctrl->free_block_head;
        ctrl->free_block_head = dir->directory[offset];
        dir->directory[offset] = target_nxt;
        
        if(dir->directory[offset] == -1){
            ctrl->dirblock_num[index]--;
            
            if(ctrl->dirblock_num[index] == 0){
                for(int i = 0; i < index; i++){
                    if(ctrl->dirblock_nxt[i] == -1 || ctrl->dirblock_nxt[i] == index){
                        ctrl->dirblock_nxt[i] = ctrl->dirblock_nxt[index];
                    }
                }
            }
        }
        release(pool, 0);
    }
    release(pool, (1 + index) * PAGE_SIZE);
    release(pool, target_addr);

    
    
}

void print_hash_table(BufferPool *pool) {
    HashMapControlBlock *ctrl = (HashMapControlBlock*)get_page(pool, 0);
    HashMapDirectoryBlock *dir_block;
    off_t block_addr, next_addr;
    HashMapBlock *block;
    int i, j;
    printf("----------HASH TABLE----------\n");
    for (i = 0; i < ctrl->max_size; ++i) {
        dir_block = (HashMapDirectoryBlock*)get_page(pool, (i / HASH_MAP_DIR_BLOCK_SIZE + 1) * PAGE_SIZE);
        if (dir_block->directory[i % HASH_MAP_DIR_BLOCK_SIZE] != -1) {
            printf("%d:", i);
            block_addr = dir_block->directory[i % HASH_MAP_DIR_BLOCK_SIZE];
            while (block_addr != -1) {
                block = (HashMapBlock*)get_page(pool, block_addr);
                printf("  [" FORMAT_OFF_T "]", block_addr);
                printf("{");
                for (j = 0; j < block->n_items; ++j) {
                    if(j > 30) break;
                    if (j != 0) {
                        printf(", ");
                    }
                    printf(FORMAT_OFF_T, block->table[j]);
                }
                printf("}");
                next_addr = block->next;
                release(pool, block_addr);
                block_addr = next_addr;
            }
            printf("\n");
        }
        release(pool, (i / HASH_MAP_DIR_BLOCK_SIZE + 1) * PAGE_SIZE);
    }
    release(pool, 0);
    printf("------------------------------\n");
}
