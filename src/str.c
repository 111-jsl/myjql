#include "str.h"

#include "table.h"

void read_string(Table *table, RID rid, StringRecord *record) {
    // printf("read string\n");
    // ItemPtr dest = NULL;
    table_read(table, rid, &record->chunk);
    // printf("size: %d\n", get_item_id_size(get_item_id(&table->data_pool, get_rid_idx(rid))));
    // record->chunk = *(StringChunk*)dest;
    record->idx = 0;
    // system("pause");
}

int has_next_char(StringRecord *record) {
    if(get_str_chunk_size(&record->chunk) > record->idx || 
    get_rid_block_addr(get_str_chunk_rid(&record->chunk)) != -1){
        return 1;
    }
    return 0;
}

char next_char(Table *table, StringRecord *record) {
    if(get_str_chunk_size(&record->chunk) == record->idx){
        // printf("next dest: %d\n", get_rid_block_addr(get_str_chunk_rid(&record->chunk)));
        read_string(table, get_str_chunk_rid(&record->chunk), record);
    }
    return *(get_str_chunk_data_ptr(&record->chunk) + record->idx++);
}

int compare_string_record(Table *table, const StringRecord *a, const StringRecord *b) {
    while(has_next_char(a) && has_next_char(b)){
        char nxt_a = next_char(table, a);
        char nxt_b = next_char(table, b);
        if(nxt_a > nxt_b){
            return 1;
        }
        else if(nxt_a < nxt_b){
            return -1;
        }
    }
    if(has_next_char(a)){
        return 1;
    }
    if(has_next_char(b)){
        return -1;
    }
    return 0;
}

RID write_string(Table *table, const char *data, off_t size) {
    off_t chunk_size = sizeof(StringChunk) - sizeof(RID) - sizeof(short);
    off_t num_of_chunk = size / chunk_size;
    off_t now_size = size % chunk_size;
    if(now_size == 0 && num_of_chunk != 0) now_size = chunk_size;
    else num_of_chunk++;
    StringChunk chunk;
    RID pre;
    get_rid_idx(pre) = -1;
    get_rid_block_addr(pre) = -1;
    off_t now_pos = size - now_size;
    // printf("num of chunk: %d\n", num_of_chunk);
    for(int i = 0; i < num_of_chunk; i++){
        char* data_ptr = get_str_chunk_data_ptr(&chunk);
        for(int j = 0; j < now_size; j++){
            data_ptr[j] = data[now_pos + j];
        }
        get_str_chunk_size(&chunk) = (short)now_size;
        get_str_chunk_rid(&chunk) = pre;
        
        pre = table_insert(table, &chunk, calc_str_chunk_size(now_size) + sizeof(ItemID));
        now_size = chunk_size;
        now_pos -= now_size;
    }
    return pre;
    
}

void delete_string(Table *table, RID rid) {
    StringChunk dest;
    RID nxt_rid;
    
    while(get_rid_block_addr(rid) != -1){
        // printf("delete rid block addr: %d\n", get_rid_block_addr(rid));
        // printf("delete rid idx: %d\n", get_rid_idx(rid));
        // printf("block_addr: %d\n", get_rid_block_addr(rid));
        // printf("idx: %d\n", get_rid_idx(rid));
        table_read(table, rid, &dest);
        nxt_rid = get_str_chunk_rid(&dest);
        table_delete(table, rid);
        rid = nxt_rid;
    }
}

/* void print_string(Table *table, const StringRecord *record) {
    StringRecord rec = *record;
    printf("\"");
    while (has_next_char(&rec)) {
        printf("%c", next_char(table, &rec));
    }
    printf("\"");
} */

size_t load_string(Table *table, const StringRecord *record, char *dest, size_t max_size) {
    size_t i = 0;
    for(; i < max_size; i++){
        if(has_next_char(record) == 0){
            break;
        }
        dest[i] = next_char(table, record);
    }
    dest[i] = 0;
    return i;
}

/* void chunk_printer(ItemPtr item, short item_size) {
    if (item == NULL) {
        printf("NULL");
        return;
    }
    StringChunk *chunk = (StringChunk*)item;
    short size = get_str_chunk_size(chunk), i;
    printf("StringChunk(");
    print_rid(get_str_chunk_rid(chunk));
    printf(", %d, \"", size);
    for (i = 0; i < size; i++) {
        printf("%c", get_str_chunk_data_ptr(chunk)[i]);
    }
    printf("\")");
} */