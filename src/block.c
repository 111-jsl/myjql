#include "block.h"

#include <stdio.h>
#include <stdlib.h>

void init_block(Block *block) {
    block->n_items = 0;
    block->head_ptr = (short)(block->data - (char*)block);
    block->tail_ptr = (short)sizeof(Block);
}

ItemPtr get_item(Block *block, short idx) {
    // printf("get item\n");
    if (idx < 0) {
        // printf("idx: %d n_items: %d\n", idx, block->n_items);
        printf("get item error: idx is out of range\n");
        return NULL;
    }
    ItemID item_id = get_item_id(block, idx);
    if (get_item_id_availability(item_id)) {
        printf("get item error: item_id is not used\n");
        return NULL;
    }
    short offset = get_item_id_offset(item_id);
    return (char*)block + offset;
}

short new_item(Block *block, ItemPtr item, short item_size) {
    // printf("new item test: size: %d\n", block->tail_ptr - block->head_ptr);
    int have_hole = (block->head_ptr - 3 * sizeof(short)) / sizeof(ItemID) - block->n_items;
    // printf("have hole: %d\n", have_hole);
    if(block->tail_ptr - item_size < block->head_ptr){
        // printf("tail: %d item size: %d head: %d\n", block->tail_ptr, item_size, block->head_ptr);
        printf("new_item error: block space overflow\n");
        // system("pause");
        return -1;
    }
    block->tail_ptr -= item_size;
    int chosen_id = 0;
    for(; chosen_id < block->n_items; chosen_id++){
        if(get_item_id_availability(get_item_id(block, chosen_id)) == 1){
            break;
        }
    }
    if(chosen_id == block->n_items){
        if(block->tail_ptr < block->head_ptr + sizeof(ItemID)){
            // printf("tail: %d item size: %d head: %d\n", block->tail_ptr, item_size, block->head_ptr);
            printf("new_item error: block space overflow\n");
            // system("pause");
            return -1;
        }
        else {
            block->head_ptr += sizeof(ItemID);
        }
    }
    block->n_items++;
    get_item_id(block, chosen_id) = compose_item_id(0, block->tail_ptr, item_size);
    ItemPtr start = (char*)block + block->tail_ptr;
    for(int i = 0; i < item_size; i++){
        *(start + i) = *(item + i);
    }
    return chosen_id;
}

void delete_item(Block *block, short idx) {
    // printer_t printer = str_printer;
    // print_block(block, printer);
    ItemID delete_item_id = get_item_id(block, idx);
    if(get_item_id_availability(delete_item_id) == 1){
        printf("delete_item warning: item already deleted\n");
        return;
    }
    // if(idx >= block->n_items){
    //     printf("delete_item error: item id out of bound\n");
    //     return;
    // }
    
    if(block->head_ptr - 3 * sizeof(short) == sizeof(ItemID) * (idx + 1)){
        block->head_ptr -= sizeof(ItemID);
        for(int i = idx - 1; i >= 0; i--){
            if(get_item_id_availability(get_item_id(block, i)) == 0){
                break;
            }
            block->head_ptr -= sizeof(ItemID);
        }
    }
    // system("pause");
    // ItemID last_item_id = get_item_id(block, block->n_items - 1);
    // short last_size = get_item_id_size(last_item_id);
    // short delete_size = get_item_id_size(delete_item_id);
    // short other_move = last_size - delete_size;
    // block->tail_ptr += delete_size;
    // block->head_ptr -= sizeof(ItemID);
    // ItemPtr last_save = (ItemPtr)malloc(last_size * sizeof(char));
    // ItemPtr last_item = (ItemPtr)block + get_item_id_offset(last_item_id);
    // printf("delete string\n");
    // for(int i = 0; i < last_size; i++){
    //     last_save[i] = *(last_item + i);
    //     printf("%c", last_save[i]);
    // }
    // printf("\n");
    // if(other_move < 0){
    //     short st = get_item_id_offset(get_item_id(block, idx + 1)) + get_item_id_size(get_item_id(block, idx + 1)) - 1;
    //     short en = get_item_id_offset(get_item_id(block, block->n_items - 2));
    //     short del_st = get_item_id_offset(get_item_id(block, idx)) + get_item_id_size(get_item_id(block, idx)) - 1;
    //     for(int i = st, j = del_st; i > en; i--, j--){
    //         *((ItemPtr)block + j) = *((ItemPtr)block + i);
    //     }
    // } else if(other_move > 0){
    //     short en = get_item_id_offset(get_item_id(block, idx + 1)) + get_item_id_size(get_item_id(block, idx + 1)) - 1;
    //     short st = get_item_id_offset(get_item_id(block, block->n_items - 2));
    //     short comp_st = st - other_move;
    //     for(int i = st, j = comp_st; i < en; i++, j++){
    //         *((ItemPtr)block + j) = *((ItemPtr)block + i);
    //     }
    // }
    // for(int i = idx + 1; i < block->n_items - 2; i++){
    //     ItemID now = get_item_id(block, i);
    //     if(get_item_id_availability(now) != 1) system("pause");
    //     get_item_id(block, i) = compose_item_id(get_item_id_availability(now), get_item_id_offset(now) - other_move, get_item_id_size(now));
    // }
    // short avail = get_item_id_availability(last_item_id);
    // short offset = get_item_id_offset(delete_item_id) - other_move;
    // for(int i = 0; i < last_size; i++){
    //     *((ItemPtr)block + offset + i) = last_save[i];
    // }
    // get_item_id(block, idx) = compose_item_id(avail, offset, last_size);
    // get_item_id(block, block->n_items - 1) = compose_item_id(1, 0, 0);
    // block->n_items--;
    // free(last_save);

    short *item_sort = (short *)malloc(sizeof(short) * (block->n_items + 2));
    short item_sort_size = 0;
    // short min_dist = PAGE_SIZE + 1;
    short delete_item_offset = get_item_id_offset(delete_item_id);
    for(short i = 0, used = 0; used < block->n_items; i++){
        if(get_item_id_availability(get_item_id(block, i))) continue;
        short item_offset = get_item_id_offset(get_item_id(block, i));
        if(item_offset < delete_item_offset){
            short j = 0;
            for(; j < item_sort_size; j++){
                if(item_offset < get_item_id_offset(get_item_id(block, item_sort[j]))) break;
            }
            for(short k = item_sort_size - 1; k >= j; k--){
                item_sort[k + 1] = item_sort[k];
            }
            item_sort[j] = i;
            item_sort_size++;
        }
        used++;
    }
    // for(int i = 0; i < item_sort_size; i++){
    //     printf("item sort %d: %d\n", i, item_sort[i]);
    // }
    // system("pause");
    short delete_item_size = get_item_id_size(delete_item_id);
    short tail = delete_item_offset + delete_item_size - 1;
    for(short i = item_sort_size - 1; i >= 0; i--){
        // system("pause");
        ItemID item_id = get_item_id(block, item_sort[i]);
        short j = 0;
        short now_head = get_item_id_offset(item_id);
        short now_size = get_item_id_size(item_id);
        for(; j < now_size; j++){
            *((ItemPtr)(block) + tail - j) = *((ItemPtr)(block) + now_head + now_size - 1 - j);
        }
        tail -= now_size;
        get_item_id(block, item_sort[i]) = compose_item_id(0, get_item_id_offset(item_id) + delete_item_size, get_item_id_size(item_id));
    }
    block->n_items--;
    block->tail_ptr += delete_item_size;
    get_item_id(block, idx) = compose_item_id(1, 0, 0);
    free(item_sort);
    // print_block(block, printer);
    // printf("delete complete: head: %d tail: %d\n", block->head_ptr, block->tail_ptr);
    // system("pause");
}

void str_printer(ItemPtr item, short item_size) {
    if (item == NULL) {
        printf("NULL");
        return;
    }
    short i;
    printf("\"");
    for (i = 0; i < item_size; ++i) {
        printf("%c", item[i]);
    }
    printf("\"");
}

void print_block(Block *block, printer_t printer) {
    short i, availability, offset, size, used;
    ItemID item_id;
    ItemPtr item;
    printf("----------BLOCK----------\n");
    printf("total = %d\n", block->n_items);
    printf("head = %d\n", block->head_ptr);
    printf("tail = %d\n", block->tail_ptr);
    for (i = 0, used = 0; used < block->n_items; ++i) {
        item_id = get_item_id(block, i);
        availability = get_item_id_availability(item_id);
        offset = get_item_id_offset(item_id);
        size = get_item_id_size(item_id);
        if (!availability) {
            item = get_item(block, i);
            used++;
        } else {
            item = NULL;
        }
        printf("%10d%5d%10d%10d\t", i, availability, offset, size);
        printer(item, size);
        printf("\n");
    }
    printf("-------------------------\n");
}

void analyze_block(Block *block, block_stat_t *stat) {
    short i;
    stat->empty_item_ids = 0;
    stat->total_item_ids = block->n_items;
    for (i = 0; i < block->n_items; ++i) {
        if (get_item_id_availability(get_item_id(block, i))) {
            ++stat->empty_item_ids;
        }
    }
    stat->available_space = block->tail_ptr - block->head_ptr 
        + stat->empty_item_ids * sizeof(ItemID);
}

void accumulate_stat_info(block_stat_t *stat, const block_stat_t *stat2) {
    stat->empty_item_ids += stat2->empty_item_ids;
    stat->total_item_ids += stat2->total_item_ids;
    stat->available_space += stat2->available_space;
}

void print_stat_info(const block_stat_t *stat) {
    printf("==========STAT==========\n");
    printf("empty_item_ids: " FORMAT_SIZE_T "\n", stat->empty_item_ids);
    printf("total_item_ids: " FORMAT_SIZE_T "\n", stat->total_item_ids);
    printf("available_space: " FORMAT_SIZE_T "\n", stat->available_space);
    printf("========================\n");
}