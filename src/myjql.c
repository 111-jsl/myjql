#include "myjql.h"

#include "buffer_pool.h"
#include "b_tree.h"
#include "table.h"
#include "str.h"

typedef struct {
    RID key;
    RID value;
} Record;

void read_record(Table *table, RID rid, Record *record) {
    table_read(table, rid, (ItemPtr)record);
}

RID write_record(Table *table, const Record *record) {
    return table_insert(table, (ItemPtr)record, sizeof(Record) + sizeof(ItemID));
}

void delete_record(Table *table, RID rid) {
    table_delete(table, rid);
}



BufferPool bp_idx;
Table tbl_rec;
Table tbl_str;

int b_tree_row_row_cmp(RID a, RID b){
    // printf("cmp begin\n");
    // print_rid(a);
    // print_rid(b);
    // printf("\n");
    Record rec_a, rec_b;
    read_record(&tbl_rec, a, &rec_a);
    read_record(&tbl_rec, b, &rec_b);
    // printf("cmp read_record complete\n");
    StringRecord str_a, str_b;
    read_string(&tbl_str, rec_a.key, &str_a);
    read_string(&tbl_str, rec_b.key, &str_b);
    // printf("cmp read_string complete\n");
    return compare_string_record(&tbl_str, &str_a, &str_b);
}

int b_tree_ptr_row_cmp(void *key, size_t key_len, RID b){
    Record rec_b;
    read_record(&tbl_rec, b, &rec_b);
    StringRecord str_b;
    read_string(&tbl_str, rec_b.key, &str_b);
    char *str_a = (char*)key;
    int i = 0;
    for(; i < key_len && has_next_char(&str_b); i++){
        char nxt_b = next_char(&tbl_str, &str_b);
        if(str_a[i] > nxt_b){
            return 1;
        }
        else if(str_a[i] < nxt_b){
            return -1;
        }
    }
    if(i < key_len){
        return 1;
    }
    if(has_next_char(&str_b)){
        return -1;
    }
    return 0;
}

void myjql_init() {
    b_tree_init("rec.idx", &bp_idx);
    table_init(&tbl_rec, "rec.data", "rec.fsm");
    table_init(&tbl_str, "str.data", "str.fsm");
}

void myjql_close() {
    /* validate_buffer_pool(&bp_idx);
    validate_buffer_pool(&tbl_rec.data_pool);
    validate_buffer_pool(&tbl_rec.fsm_pool);
    validate_buffer_pool(&tbl_str.data_pool);
    validate_buffer_pool(&tbl_str.fsm_pool); */
    b_tree_close(&bp_idx);
    table_close(&tbl_rec);
    table_close(&tbl_str);
}




size_t myjql_get(const char *key, size_t key_len, char *value, size_t max_size) {
    b_tree_ptr_row_cmp_t prcmp = b_tree_ptr_row_cmp;
    RID rec_addr = b_tree_search(&bp_idx, key, key_len, prcmp);
    if(get_rid_block_addr(rec_addr) == -1){
        return -1;
    }
    // print_rid(rec_addr);
    // printf("---------------b_tree_search complete\n");
    Record get_rec;
    read_record(&tbl_rec, rec_addr, &get_rec);
    // print_rid(get_rec.key);
    // print_rid(get_rec.value);
    // printf("\n");
    // printf("---------------read_record complete\n");
    StringRecord get_str;
    read_string(&tbl_str, get_rec.value, &get_str);
    // printf("---------------read_string complete\n");
    return load_string(&tbl_str, &get_str, value, max_size);
}

void myjql_set(const char *key, size_t key_len, const char *value, size_t value_len) {
    b_tree_ptr_row_cmp_t prcmp = b_tree_ptr_row_cmp;
    // b_tree_traverse(&bp_idx);
    RID rec_addr = b_tree_search(&bp_idx, key, key_len, prcmp);
    RID save_key_addr;
    // printf("\n");
    // printf("res:");
    // print_rid(rec_addr);
    // printf("\n");
    // printf("------------------b_tree_search complete\n");
    if(get_rid_block_addr(rec_addr) != -1){
        Record rec;
        b_tree_row_row_cmp_t rrcmp = b_tree_row_row_cmp;
        b_tree_delete(&bp_idx, rec_addr, rrcmp, NULL, NULL);
        // printf("---------------b_tree_delete complete\n");
        read_record(&tbl_rec, rec_addr, &rec);
        // printf("---------------read_record complete\n");
        // print_rid(rec.key);
        // print_rid(rec.value);
        save_key_addr = rec.key;
        // printf("---------------key delete_string complete\n");
        delete_string(&tbl_str, rec.value);
        // printf("---------------value delete_string complete\n");
        delete_record(&tbl_rec, rec_addr);
        // printf("---------------delete_record complete\n");
        
        
    }
    
    RID value_addr = write_string(&tbl_str, value, value_len);
    // printf("write_string value_addr: ");
    // print_rid(value_addr);
    // printf("\n");
    // printf("---------------value write_string complete\n");
    RID key_addr;
    if(get_rid_block_addr(rec_addr) != -1) key_addr = save_key_addr;
    else key_addr = write_string(&tbl_str, key, key_len);
    // printf("write_string key_addr: ");
    // print_rid(key_addr);
    // printf("\n");
    // printf("---------------key write_string complete\n");
    Record rec;
    rec.key = key_addr;
    rec.value = value_addr;
    RID row_ptr = write_record(&tbl_rec, &rec);
    // printf("---------------write_record complete\n");
    b_tree_row_row_cmp_t rrcmp = b_tree_row_row_cmp;
    b_tree_insert(&bp_idx, row_ptr, rrcmp, NULL);
    // printf("b_tree_insert rec_addr: ");
    // print_rid(row_ptr);
    // printf("\n");
    // printf("---------------b_tree_insert complete\n");

}

void myjql_del(const char *key, size_t key_len) {
    b_tree_ptr_row_cmp_t prcmp = b_tree_ptr_row_cmp;
    RID rec_addr = b_tree_search(&bp_idx, key, key_len, prcmp);
    if(get_rid_block_addr(rec_addr) == -1){
        return;
    }
    // printf("---------------b_tree_search complete\n");
    Record rec;
    b_tree_row_row_cmp_t rrcmp = b_tree_row_row_cmp;
    b_tree_delete(&bp_idx, rec_addr, rrcmp, NULL, NULL);
    // printf("---------------b_tree_delete complete\n");
    read_record(&tbl_rec, rec_addr, &rec);
    // printf("---------------read_record complete\n");
    delete_string(&tbl_str, rec.key);
    // printf("---------------key delete_string complete\n");
    delete_string(&tbl_str, rec.value);
    // printf("---------------value delete_string complete\n");
    delete_record(&tbl_rec, rec_addr);
    // printf("---------------delete_record complete\n");
    
}

/* void myjql_analyze() {
    printf("Record Table:\n");
    analyze_table(&tbl_rec);
    printf("String Table:\n");
    analyze_table(&tbl_str);
} */