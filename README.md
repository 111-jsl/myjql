# 数据库引论PJ2优化细节



## 分析问题

- 一开始想的是降低总请求数以及miss占比来提速

## 数据分析

- 发现`test_hash_map`中第三个测试，`hash_map_pop_lower_bound`的请求偏高，在目录较空的情况下，每次从请求的`size`找到符合条件的`size`需要遍历，有优化的空间
- 发现`test_hash_map`中第四个测试，`hash_map_pop`函数总请求明显高于其他的函数，且miss率极高，平均每次调用产生30次请求

下面主要是优化问题1，问题2涉及链表过长，没有想出好的方法。

## 解决方法细节

- 增加两个数组

```c++
typedef struct {
    off_t free_block_head;
    off_t n_directory_blocks;
    off_t max_size;
    
    short dirblock_nxt[8];
    short dirblock_num[8];
} HashMapControlBlock;


```

- `dirblock_nxt[8]`维护当前块后（不包括当前块）下一个有地址的size的`dirblock`

  eg： `dirblock_nxt[2] = 5`，意思是第二个`HashMapDirectoryBlock`后面，第三个，第四个的`directory[size] = -1`

  ​		`dirblock_nxt[3] = -1`，就是第三块后面都是空的

- 初始化

```c++
for(int i = 0; i < 8; i++){
    ctrl->dirblock_nxt[i] = -1;
    ctrl->dirblock_num[i] = 0;
}
```

- `hash_map_insert`函数里若`directory[request_size] == -1`且`dirblock_num[current_block_id] == 0`说明这是当前块里第一个插入的地址，需要将之前的块的`dirblock_nxt`更新为当前块

```c++
if(new_size_append){
    // index即current_block_id
    ctrl->dirblock_num[index]++;
    for(int i = 0; i < index; i++){
        // 更新dirbock_nxt数组
        if(ctrl->dirblock_nxt[i] == -1 || ctrl->dirblock_nxt[i] > index) ctrl->dirblock_nxt[i] = index;
    }
}
```

- `hash_map_pop_lower_bound`函数最多只需要请求两个`HashMapDirectoryBlock`

​			1、首先根据参数`size`，确定一开始的`dirblock`，然后遍历，找到符合条件的地址后，则退出

​			2、如果没找到，那么查看`dirblock_nxt[current_block_id]`

​					（1）若为-1，则返回-1

​					（2）若不为-1，则请求目标目录块

​			3、找到符合条件的地址，需要更新数组

```c++
	if(now->n_items - 1 == 0){
	
	// ...
	
		if(dir->directory[offset] == -1){
            ctrl->dirblock_num[index]--;
            if(ctrl->dirblock_num[index] == 0){
                for(int i = 0; i < index; i++){
                    if(ctrl->dirblock_nxt[i] == -1 || ctrl->dirblock_nxt[i] == index){
                        ctrl->dirblock_nxt[i] = ctrl->dirblock_nxt[index];
                    }
                }
            }
        }
     }
```

- `hash_map_pop`函数只需考虑更新数组，具体操作同`pop_lower_bound`的第三条



## 方案效果

#### 直观上来看：

- 维护的代价：`insert`和`pop`的过程中，需要在`HashMapControlBlock`中的数组遍历至多8次；
- 节省的开销：`pop_lower_bound`中至多需要遍历两个`HashMapDirectoryBlock`；

也就是每次使用`pop_lower_bound`函数几乎零成本节省平均 `6 / 2 = 3 `个`get_page`的调用；



#### 测试时间上来看：

发现随着`Page_size`的增大，优化的效果会更明显，原因考虑到是换页的代价变大。

测试参数：

`Page_size = 16384`

`test_hash_map：test(1000000, 1, 1, 1, 1, 1, 1, 0)`

`test_str：test(512, 100000, 1)`

 未优化：

```
 	Start 1: test_hash_map
1/3 Test #1: test_hash_map ....................   Passed  137.42 sec
    Start 2: test_str
2/3 Test #2: test_str .........................   Passed   19.39 sec
```

优化：

```
 	Start 1: test_hash_map
1/3 Test #1: test_hash_map ....................   Passed   75.06 sec
    Start 2: test_str
2/3 Test #2: test_str .........................   Passed   16.98 sec
```



对照组：

`Page_size = 4096`

`test_hash_map：test(1000000, 1, 1, 1, 1, 1, 1, 0)`

`test_str：test(512, 100000, 1)`

 未优化：

```
 	Start 1: test_hash_map
1/3 Test #1: test_hash_map ....................   Passed  113.85 sec
    Start 2: test_str
2/3 Test #2: test_str .........................   Passed   19.44 sec
```

优化：

```
 	Start 1: test_hash_map
1/3 Test #1: test_hash_map ....................   Passed   60.64 sec
    Start 2: test_str
2/3 Test #2: test_str .........................   Passed   17.57 sec
```

对照组和第一组优化时间差距10s，验证了优化效果和`Page_size`大小的关系



#### 测试环境：

```
hardware:
CPU:i5-8300H
RAM:8GB
SSD文件系统:NTFS

software:
OS:windows 11
VS:2022 / VScode
```

